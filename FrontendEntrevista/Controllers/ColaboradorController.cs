﻿using FrontendEntrevista.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FrontendEntrevista.Controllers
{
    public class ColaboradorController : Controller
    {
        // GET: Colaborador
        public async Task<ActionResult> Index()
        {
            var model = new List<Colaborador>();
            // Utilizamos el objeto System.Net.HttpClient para consumir el servicio REST
            using (var client = new HttpClient())
            {
                string ApiAddress = "https://localhost:44379/Api";
                string BusquedaUri = $"{ApiAddress}/Colaborador";

                // Establecemos la dirección base del servicio REST
                client.BaseAddress = new Uri(ApiAddress);

                // Limpiamos Encabezados de la petición
                client.DefaultRequestHeaders.Accept.Clear();

                // Hacemos una peticion POST al servicio enviando el objeto JSON
                var response = await client.GetAsync(BusquedaUri);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // Leemos el resultado devuelto
                    var resultWebApi = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<List<Colaborador>>(resultWebApi);
                    model = data;
                }
            }

            return View(model);
        }

        // GET: Colaborador/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var model = new Colaborador();
            // Utilizamos el objeto System.Net.HttpClient para consumir el servicio REST
            using (var client = new HttpClient())
            {
                string ApiAddress = "https://localhost:44379/Api";
                string BusquedaUri = $"{ApiAddress}/Colaborador/"+id;

                // Establecemos la dirección base del servicio REST
                client.BaseAddress = new Uri(ApiAddress);

                // Limpiamos Encabezados de la petición
                client.DefaultRequestHeaders.Accept.Clear();

                // Hacemos una peticion POST al servicio enviando el objeto JSON
                var response = await client.GetAsync(BusquedaUri);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // Leemos el resultado devuelto
                    var resultWebApi = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Colaborador>(resultWebApi);
                    model = data;
                }
            }
            
            return View(model);
        }

        // GET: Colaborador/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Colaborador/Create
        [HttpPost]
        public async Task<ActionResult> Create(Colaborador collection)
        {
            try
            {
                // Utilizamos el objeto System.Net.HttpClient para consumir el servicio REST
                using (var client = new HttpClient())
                {
                    string ApiAddress = "https://localhost:44379/Api";
                    string BusquedaUri = $"{ApiAddress}/Colaborador";

                    // Establecemos la dirección base del servicio REST
                    client.BaseAddress = new Uri(ApiAddress);

                    // Limpiamos Encabezados de la petición
                    client.DefaultRequestHeaders.Accept.Clear();

                    // Hacemos una peticion POST al servicio enviando el objeto JSON
                    var json = JsonConvert.SerializeObject(collection);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(BusquedaUri, data);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                    }
                }


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Colaborador/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var model = new Colaborador();
            // Utilizamos el objeto System.Net.HttpClient para consumir el servicio REST
            using (var client = new HttpClient())
            {
                string ApiAddress = "https://localhost:44379/Api";
                string BusquedaUri = $"{ApiAddress}/Colaborador/" + id;

                // Establecemos la dirección base del servicio REST
                client.BaseAddress = new Uri(ApiAddress);

                // Limpiamos Encabezados de la petición
                client.DefaultRequestHeaders.Accept.Clear();

                // Hacemos una peticion POST al servicio enviando el objeto JSON
                var response = await client.GetAsync(BusquedaUri);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // Leemos el resultado devuelto
                    var resultWebApi = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Colaborador>(resultWebApi);
                    model = data;
                }
            }

            return View(model);
        }

        // POST: Colaborador/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, FormCollection collection)
        {
            try
            {
                // Utilizamos el objeto System.Net.HttpClient para consumir el servicio REST
                using (var client = new HttpClient())
                {
                    string ApiAddress = "https://localhost:44379/Api";
                    string BusquedaUri = $"{ApiAddress}/Colaborador"+id;

                    // Establecemos la dirección base del servicio REST
                    client.BaseAddress = new Uri(ApiAddress);

                    // Limpiamos Encabezados de la petición
                    client.DefaultRequestHeaders.Accept.Clear();

                    // Hacemos una peticion POST al servicio enviando el objeto JSON
                    var json = JsonConvert.SerializeObject(collection);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = await client.PutAsync(BusquedaUri, data);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Colaborador/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Colaborador/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            try
            {
                // Utilizamos el objeto System.Net.HttpClient para consumir el servicio REST
                using (var client = new HttpClient())
                {
                    string ApiAddress = "https://localhost:44379/Api";
                    string BusquedaUri = $"{ApiAddress}/Colaborador" + id;

                    // Establecemos la dirección base del servicio REST
                    client.BaseAddress = new Uri(ApiAddress);

                    // Limpiamos Encabezados de la petición
                    client.DefaultRequestHeaders.Accept.Clear();

                    // Hacemos una peticion POST al servicio enviando el objeto JSON
                    var json = JsonConvert.SerializeObject(collection);

                    var response = await client.DeleteAsync(BusquedaUri);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

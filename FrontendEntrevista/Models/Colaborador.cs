﻿using System;

namespace FrontendEntrevista.Models
{
    public class Colaborador 
    {
        public int ColaboradorId { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string EstadoCivil { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string GradoAcademico { get; set; }
        public string Direccion { get; set; }
    }
}
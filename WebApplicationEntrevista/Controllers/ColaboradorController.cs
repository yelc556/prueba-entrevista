﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using WebApplicationEntrevista.Conexion;
using WebApplicationEntrevista.Constantes;
using WebApplicationEntrevista.Models;
using WebApplicationSistemaCableInternet.Utilitarios;

namespace WebApplicationEntrevista.Controllers
{
    public class ColaboradorController : ApiController
    {
        private readonly DbConexion db = new DbConexion();
        private Utilitario utilitario = new Utilitario();

        // GET api/Colaborador/

        public IEnumerable<Colaborador> Get()
        {
            List<Colaborador> colaboradores = new List<Colaborador>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_ConsultarColabores
                };

                colaboradores  = this.utilitario.ParserListaObjeto<List<Colaborador>>(db.Conexion(sqlCommand));
            }
            catch (Exception ex)
            {
                // Ocurrio un error al consultar la informacion;
            }
            return colaboradores;
        }

        // GET api/Colaborador/5
        public Colaborador Get(int id)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_ConsultarColabores
                };
                sqlCommand.Parameters.AddWithValue("@ColaboradorId", id);

                return this.utilitario.ParserObjeto<Colaborador>(db.Conexion(sqlCommand));
            }
            catch (Exception ex)
            {
                // Ocurrio un error al consultar la informacion;
                return null;
            }
        }

        public Colaborador Post([FromBody] Colaborador colaborador)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_CrearColabores
                };
                sqlCommand.Parameters.AddWithValue("@Nombres", colaborador.Nombres);
                sqlCommand.Parameters.AddWithValue("@Apellidos", colaborador.Apellidos);
                sqlCommand.Parameters.AddWithValue("@FechaNacimiento", colaborador.FechaNacimiento.ToString("dd/MM/yyyy"));
                sqlCommand.Parameters.AddWithValue("@EstadoCivil", colaborador.EstadoCivil.ToString());
                sqlCommand.Parameters.AddWithValue("@GradoAcademico", colaborador.GradoAcademico);
                sqlCommand.Parameters.AddWithValue("@Direccion", colaborador.Direccion);
                
                return this.utilitario.ParserObjeto<Colaborador>(db.Conexion(sqlCommand));
            }
            catch (Exception ex)
            {
                // Ocurrio un error al consultar la informacion;
                return null;
            }
        }

        public Colaborador Put(int id, [FromBody] Colaborador colaborador)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_CrearColabores
                };
                sqlCommand.Parameters.AddWithValue("@ColaboradorId", id);
                sqlCommand.Parameters.AddWithValue("@Nombres", colaborador.Nombres);
                sqlCommand.Parameters.AddWithValue("@Apellidos", colaborador.Apellidos);
                sqlCommand.Parameters.AddWithValue("@FechaNacimiento", colaborador.FechaNacimiento.ToString("dd/MM/yyyy"));
                sqlCommand.Parameters.AddWithValue("@EstadoCivil", colaborador.EstadoCivil.ToString());
                sqlCommand.Parameters.AddWithValue("@GradoAcademico", colaborador.GradoAcademico);
                sqlCommand.Parameters.AddWithValue("@Direccion", colaborador.Direccion);

                return this.utilitario.ParserObjeto<Colaborador>(db.Conexion(sqlCommand));
            }
            catch (Exception ex)
            {
                // Ocurrio un error al consultar la informacion;
                return null;
            }
        }

        // Delete api/Colaborador/5
        public int Delete(int id)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_BorrarColabores
                };
                sqlCommand.Parameters.AddWithValue("@ColaboradorId", id);
                db.Conexion(sqlCommand);
                return id;
            }
            catch (Exception ex)
            {
                // Ocurrio un error al consultar la informacion;
                return 0;
            }
        }

    }
}

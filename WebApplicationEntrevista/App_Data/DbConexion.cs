﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebApplicationEntrevista.Conexion
{
    class DbConexion
    {
        private bool ocurrioError;
        private string mensajeError;

        public DbConexion()
        {
            this.ocurrioError = false;
            this.mensajeError = string.Empty;
        }

        public bool OcurrioError
        {
            get { return this.ocurrioError; }
        }

        public string MensajeError
        {
            get { return this.mensajeError; }
        }

        public DataTable Conexion(SqlCommand sqlCommand)
        {

            DataTable data = new DataTable();
            try
            {
                string connectionString = "Server = YELC\\SQLEXPRESS01; Initial Catalog = entrevistaDB; Persist Security Info = False; User ID = dev; Password = ne56r10u; MultipleActiveResultSets = False; Encrypt = False; TrustServerCertificate = False; Connection Timeout = 30; ";

                //string connectionString = ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString;
                using (SqlConnection connectionDb = new SqlConnection(connectionString))
                {
                    connectionDb.Open();
                    sqlCommand.Connection = connectionDb;
                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    data = new DataTable();
                    data.Load(dataReader);
                }
            }
            catch (Exception ex)
            {
                this.ocurrioError = true;
                this.mensajeError = ex.Message;
            }

            return data;
        }
    }
}

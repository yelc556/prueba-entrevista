﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace WebApplicationSistemaCableInternet.Utilitarios
{
    public class Utilitario
    {
        public T ParserListaObjeto<T>(DataTable data) where T: new ()
        {
            if (data == null)
            {
                return new T();
            }

            try
            {
                string json = JsonConvert.SerializeObject(data);
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                return new T();
            }
        }

        public T ParserObjeto<T>(DataTable data) where T : new()
        {
            if (data == null)
            {
                return new T();
            }

            try
            {
                string json = JsonConvert.SerializeObject(data);
                List<T> model = JsonConvert.DeserializeObject<List<T>>(json);
                return model[0];
            }
            catch (Exception ex)
            {
                return new T();
            }
        }
    }
}

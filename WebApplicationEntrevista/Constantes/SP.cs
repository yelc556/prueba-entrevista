﻿namespace WebApplicationEntrevista.Constantes
{
    public static class SP
    {
        // Procedimientos de Colaboradores
        public const string SP_ConsultarColabores = "SP_ListarColabores";
        public const string SP_CrearColabores = "SP_CrearColaborador";
        public const string SP_EditarColabores = "SP_EditarColaborador";
        public const string SP_BorrarColabores = "SP_BorrarColaborador";
    }
}
